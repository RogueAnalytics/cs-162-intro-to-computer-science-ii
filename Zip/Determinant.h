/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB_1_DETERMINANT_HPP
#define LAB_1_DETERMINANT_HPP


double determinant(int **matrix, int size);


#endif //LAB_1_DETERMINANT_HPP
