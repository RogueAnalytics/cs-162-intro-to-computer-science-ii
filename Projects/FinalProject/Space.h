/*********************************************************************
** Program name: Travel Oregon
** Author: W. Aaron Morris
** Date:
** Description: A Base Space for
*********************************************************************/

#ifndef FINALPROJECT_SPACE_H
#define FINALPROJECT_SPACE_H

#include <string>

class Space {
protected:
    std::string locationName;
    bool showName;
    Space *north;
    Space *south;
    Space *east;
    Space *west;
public:
    void visitSpace();
    void printSpace();
    void setSpace(Space *space, int direction);
    Space *getSpace(int direction);
};


#endif //FINALPROJECT_SPACE_H
