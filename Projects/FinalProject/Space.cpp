/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Space.h"

void Space::visitSpace() {

}

void Space::printSpace() {

}

void Space::setSpace(Space *space, int direction) {
    switch (direction){
        case 1:
            north = space;
            space->setSpace(this, 2);
            break;
        case 2:
            south = space;
            space->setSpace(this, 1);
            break;
        case 3:
            east = space;
            space->setSpace(this, 4);
            break;
        case 4:
            west = space;
            space->setSpace(this, 3);
            break;
    }
}

Space *Space::getSpace(int direction) {
    switch (direction){
        case 1:
            return north;
        case 2:
            return south;
        case 3:
            return east;
        case 4:
            return west;
    }
}
