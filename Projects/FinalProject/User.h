/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef FINALPROJECT_USER_H
#define FINALPROJECT_USER_H


#include "Space.h"

class User {
private:
    int SightSeen = 0;
    int health = 100;
    Space *space;
public:
    void move();
    void decreaseHealth(int points);
};


#endif //FINALPROJECT_USER_H
