/*********************************************************************
** Program name: Langston Ants
** Author:
** Date:
** Description: A space for keeping track of all the items on a Langston board
 * (i.e. an Ant and Spaces).
*********************************************************************/

#ifndef PROJECT_1_BOARD_H
#define PROJECT_1_BOARD_H


#include "Space.h"
#include "User.h"

class Map {
private:
    Space ***spaces;
    User *User;

public:
    Map();
    virtual ~Map();
    void printMap();
    void runMap();



};


#endif //PROJECT_1_BOARD_H
