cmake_minimum_required(VERSION 3.12)
project(FinalProject)

set(CMAKE_CXX_STANDARD 14)

add_executable(FinalProject main.cpp Space.cpp Space.h User.cpp User.h PointOfInterest.cpp PointOfInterest.h City.cpp City.h Hazard.cpp Hazard.h utility.cpp utility.h Map.cpp Map.h)