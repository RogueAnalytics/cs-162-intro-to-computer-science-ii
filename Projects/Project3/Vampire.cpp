/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Vampire
*********************************************************************/

#include "Vampire.h"
#include <iostream>
#include <random>

Vampire::Vampire() : Character("Vampire", 1, 12, 1, 6, 1, 18) {}

int Vampire::attack() {
    int attackPower = 0;
    for(int i = 0; i < attackDiceCount; i++) {
        attackPower += attackDice.roll();
    }

    return attackPower;
}

int Vampire::defend(int attackPower) {
    int defensePower = 0;

    if (rand()%2==0){
        for(int i = 0; i < defenseDiceCount; i++) {
            defensePower += defenseDie.roll();
        }

        std::cout << defensePower << std::endl;
        defensePower += armor;

        if((attackPower - defensePower) > 0) {
            std::cout << "\tDamage Inflicted: " << (attackPower - defensePower) << std::endl;
            strength -= (attackPower - defensePower);
        } else {
            std::cout << "\tDamage Inflicted: 0" << std::endl;
        }
    } else {
        std::cout << "The Vampire has charmed his opponent"<< std::endl;
        std::cout << "\tDamage Inflicted: 0" << std::endl;
    }

    return defensePower;
}

