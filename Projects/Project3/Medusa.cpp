/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Medusa
*********************************************************************/

#include "Medusa.h"
#include <iostream>
#include <climits>

Medusa::Medusa() : Character("Medusa", 2, 6, 1, 6, 3, 8) {}

int Medusa::attack() {
    int attackPower = 0;
    for(int i = 0; i < attackDiceCount; i++) {
        attackPower += attackDice.roll();
    }

    if (attackPower == 12){
        std::cout << "Medusa glared at her opponent!!!" << std::endl;
        attackPower = INT_MAX;
    }

    return attackPower;
}

int Medusa::defend(int attackPower) {
    int defensePower = 0;

    for(int i = 0; i < defenseDiceCount; i++) {
        defensePower += defenseDie.roll();
    }

    std::cout << defensePower << std::endl;

    defensePower += armor;

    if((attackPower - defensePower) > 0) {
        std::cout << "\tDamage Inflicted: " << (attackPower - defensePower) << std::endl;
        strength -= (attackPower - defensePower);
    } else {
        std::cout << "\tDamage Inflicted: 0" << std::endl;
    }

    return defensePower;
}
