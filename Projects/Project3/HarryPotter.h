/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Harry Potter
*********************************************************************/

#ifndef PROJECT3_HARRYPOTTER_H
#define PROJECT3_HARRYPOTTER_H

#include "Character.h"

class HarryPotter : public Character {
private:
    int deathCount=0;
public:
    HarryPotter();

    int attack() override;

    int defend(int attackPower) override;
};


#endif //PROJECT3_HARRYPOTTER_H
