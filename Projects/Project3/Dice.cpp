/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Dice
*********************************************************************/

#include "Dice.h"
#include <random>

int Dice::roll() {
    return (rand() % sides) + 1;
}

Dice::Dice(int sides) : sides(sides) {}
