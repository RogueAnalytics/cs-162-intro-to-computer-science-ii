/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Vampire
*********************************************************************/

#ifndef PROJECT3_VAMPIRE_H
#define PROJECT3_VAMPIRE_H

#include "Character.h"

class Vampire : public Character {
public:
    Vampire();

    int attack() override;

    int defend(int attackPower) override;

};


#endif //PROJECT3_VAMPIRE_H
