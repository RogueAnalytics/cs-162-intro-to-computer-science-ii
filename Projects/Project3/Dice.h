/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Dice
*********************************************************************/

#ifndef PROJECT3_DICE_H
#define PROJECT3_DICE_H


class Dice {
private:
    int sides;
public:
    Dice() = default;
    /*!
     * Basic Constructor
     * @param sides
     */
    explicit Dice(int sides);

    /*!
     * Return the roll of the dice
     * @return
     */
    int roll();
};


#endif //PROJECT3_DICE_H
