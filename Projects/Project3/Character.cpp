/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of a base chracter
*********************************************************************/

#include "Character.h"

#include <iostream>

int Character::getStrength() const {
    if (strength < 0){
        return 0;
    } else {
        return strength;
    }
}


Character::Character(std::string type, int attackDiceCount, int aDiceSides,
                     int defenseDiceCount, int dDiceSides, int arm, int str) {
    this->characterType = type;
    this->attackDiceCount = attackDiceCount;
    this->attackDice = Dice(aDiceSides);
    this->defenseDiceCount = defenseDiceCount;
    this->defenseDie = Dice(dDiceSides);
    this->armor = arm;
    this->strength = str;
}

const std::string Character::getCharacterType() const {
    return characterType;
}

int Character::getArmor() const {
    return armor;
}

void Character::printDefenseStats() {
    std::cout << "\tStrength: " << strength <<std::endl;
    std::cout << "\tArmor: " << armor <<std::endl;
}

Character::~Character() {}

