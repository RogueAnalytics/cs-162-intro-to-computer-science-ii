/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of  Character Factory
*********************************************************************/

#include "CharFactory.h"
#include "Character.h"
#include "utility.h"
#include "Barbarian.h"
#include "BlueMen.h"
#include "Medusa.h"
#include "HarryPotter.h"
#include "Vampire.h"

Character *getCharacter(){
    int cha;
    utility::getMinMaxInt(cha, 1, 5, "Select your Character:"
                                     "\n\t1.) Barbarian"
                                     "\n\t2.) Blue Men"
                                     "\n\t3.) Harry Potter"
                                     "\n\t4.) Medusa"
                                     "\n\t5.) Vampire",
                                     "Invalid Choice");

    if (cha == 1){
        return new Barbarian();
    } else if (cha == 2){
        return new BlueMen();
    } else if (cha == 3){
        return new HarryPotter();
    } else if (cha == 4){
        return new Medusa();
    } else if (cha == 5){
        return new Vampire();
    }
    return nullptr;
}
