/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Character Factory
*********************************************************************/

#ifndef PROJECT3_CHARFACTORY_H
#define PROJECT3_CHARFACTORY_H

#include "Character.h"

Character *getCharacter();

#endif //PROJECT3_CHARFACTORY_H
