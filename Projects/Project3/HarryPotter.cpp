/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Harry Potter
*********************************************************************/

#include "HarryPotter.h"
#include <iostream>

HarryPotter::HarryPotter() : Character("Harry Potter", 2, 6, 2, 6, 0, 10) {}

int HarryPotter::attack() {
    int attackPower = 0;
    for(int i = 0; i < attackDiceCount; i++) {
        attackPower += attackDice.roll();
    }

    return attackPower;
}

int HarryPotter::defend(int attackPower) {
    int defensePower = 0;

    for(int i = 0; i < defenseDiceCount; i++) {
        defensePower += defenseDie.roll();
    }

    std::cout << defensePower << std::endl;

    defensePower += armor;

    if((attackPower - defensePower) > 0) {
        std::cout << "\tDamage Inflicted: " << (attackPower - defensePower) << std::endl;
        strength -= (attackPower - defensePower);
    } else {
        std::cout << "\tDamage Inflicted: 0" << std::endl;
    }

    if(strength <= 0 && deathCount==0){
        std::cout << "Harry Potter was resurrected" << std::endl;
        strength = 20;
        deathCount++;
    }

    return defensePower;
}
