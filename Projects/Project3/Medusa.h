/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Medusa
*********************************************************************/

#ifndef PROJECT3_MEDUSA_H
#define PROJECT3_MEDUSA_H

#include "Character.h"

class Medusa : public Character {
public:
    Medusa();

    /*!
     * The calculation of the attack of the Barbarian
     * @return
     */
    int attack() override;
    /*!
     * Defend an attack and calculate damage.
     * @param attack The attack the character is defending
     */
    int defend(int attackPower) override;
};


#endif //PROJECT3_MEDUSA_H
