/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of a base character
*********************************************************************/

#ifndef PROJECT3_CHARACTER_H
#define PROJECT3_CHARACTER_H

#include <string>
#include "Dice.h"

class Character {
protected:
    std::string characterType;

    Dice attackDice;
    int attackDiceCount;

    Dice defenseDie;
    int defenseDiceCount;

    int armor;
    int strength;

public:
    /*!
     *
     * @param attackDiceCount Number of Attack Dice
     * @param aDiceSides Number of Sides for Attack Dice
     * @param defenseDiceCount Number of Defense Dice
     * @param dDiceSides Number of Sides for Defense Dice
     * @param arm Armor
     * @param str Strength
     */
    Character(std::string type, int attackDiceCount, int aDiceSides,
              int defenseDiceCount, int dDiceSides, int arm, int str);

    virtual ~Character();

    /*!
     * Get the Characters Strength
     * @return
     */
    int getStrength() const;

    int getArmor() const;

    const std::string getCharacterType() const;

    void printDefenseStats();

    void printAttack();


    //Pure virtual methods
    /*!
     * Generate Attack Power
     */
    virtual int attack() = 0;
    /*!
     * Calculate Defense and Determine Loss in HP
     * @param attackNum
     */
    virtual int defend(int attackPower) = 0;

};


#endif //PROJECT3_CHARACTER_H
