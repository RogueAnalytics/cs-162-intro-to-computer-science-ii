/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Blue Men
*********************************************************************/

#include "BlueMen.h"
#include <iostream>


BlueMen::BlueMen() : Character("Blue Men", 2, 10, 3, 6, 3, 12) {}

int BlueMen::attack() {
    int attackPower = 0;
    for(int i = 0; i < attackDiceCount; i++) {
        attackPower += attackDice.roll();
    }

    return attackPower;
}

int BlueMen::defend(int attackPower) {
    int defensePower = 0;

    for(int i = 0; i < defenseDiceCount; i++) {
        defensePower += defenseDie.roll();
    }

    std::cout << defensePower << std::endl;

    defensePower += armor;

    if((attackPower - defensePower) > 0) {
        std::cout << "\tDamage Inflicted: " << (attackPower - defensePower) << std::endl;
        strength -= (attackPower - defensePower);


        if (strength <= 4){
            std::cout << "Member(s) of the Blue Man Group fell." << std::endl;
            defenseDiceCount = 1;
        } else if (strength <= 8) {
            std::cout << "Member(s) of the Blue Man Group fell." << std::endl;
            defenseDiceCount = 2;
        } else if (strength <= 12){
            std::cout << "Member(s) of the Blue Man Group fell." << std::endl;
            defenseDiceCount = 3;
        }
    } else {
        std::cout << "\tDamage Inflicted: 0" << std::endl;
    }

    return defensePower;
}

