#include <iostream>
#include "utility.h"
#include "CharFactory.h"

int main() {
    int round;
    int attackPower;
    int defensePower;
    int playAgain;
    bool gameOver=false;
    std::cout << "Welcome to FANTASY COMBAT (Project 3)" << std::endl;


    do {
        //Create all necessary objects, pointers, and string values
        Character *char1 = getCharacter();
        Character *char2 = getCharacter();

        //This do-while loop simulates 1 round of the game
        round = 1;
        gameOver = false;

        while (!gameOver){
            std::cout << "-----------------Round " << round << "-----------------" << std::endl;
            std::cout << char1->getCharacterType() << " (Character 1) attacks" << std::endl;
            std::cout << char2->getCharacterType() << " (Character 2) defends " << std::endl;
            char2->printDefenseStats();
            //Character 1 Attacks
            attackPower = char1->attack();
            std::cout << char1->getCharacterType() << " (Character 1) Attack Roll: " << attackPower << std::endl;
            std::cout << char2->getCharacterType() << " (Character 2) Defense Roll:  " ;
            defensePower = char2->defend(attackPower);

            std::cout << char2->getCharacterType() << " (Character 2) Strength:  " << char2->getStrength() << std::endl;
            if (char2->getStrength() <= 0) {
                gameOver = true;
                std::cout << char2->getCharacterType() << " (Character 2) is defeated by "
                          << char1->getCharacterType() << " (Character 1)." << std::endl;
            }


            if (!gameOver){
                std::cout << char2->getCharacterType() << " (Character 2) attacks" << std::endl;
                std::cout << char1->getCharacterType() << " (Character 1) defends " << std::endl;
                char1->printDefenseStats();
                attackPower = char2->attack();
                std::cout << char2->getCharacterType() << " (Character 2) Attack Roll: " << attackPower << std::endl;
                std::cout << char1->getCharacterType() << " (Character 1) Defense Roll:  ";
                defensePower = char1->defend(attackPower);

                std::cout << char1->getCharacterType() << " (Character 1) Strength:  " << char1->getStrength() << std::endl;
                if(char1->getStrength() <= 0) {
                    gameOver = true;
                    std::cout << char1->getCharacterType() << " (Character 1) is defeated by "
                              << char2->getCharacterType() << " (Character 2)." << std::endl;
                }
            }
            round++;
        }

        if(char1->getStrength() > char2->getStrength()){
            std::cout << "Character 1 WON" << std::endl;
        } else {
            std::cout << "Character 2 WON" << std::endl;
        }

        utility::getMinMaxInt(playAgain, 1, 2, "Do you wish to play again?"
                                               "\n\t1.) Yes"
                                               "\n\t2.) No",
                                               "Invalid Choice");


        delete char1;
        delete char2;
    } while (playAgain == 1);


}