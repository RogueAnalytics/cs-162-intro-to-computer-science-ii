/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Blue Men
*********************************************************************/

#ifndef PROJECT3_BLUEMEN_H
#define PROJECT3_BLUEMEN_H

#include "Character.h"

class BlueMen: public Character {
public:
    BlueMen();

    int attack() override;

    int defend(int attackNum) override;
};


#endif //PROJECT3_BLUEMEN_H
