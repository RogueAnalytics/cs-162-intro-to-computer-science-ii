/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Barbarian
*********************************************************************/

#include "Barbarian.h"

#include <iostream>

Barbarian::Barbarian(): Character("Barbarian", 2, 6, 2, 6, 0, 12) {}

int Barbarian::attack() {
    int attackPower = 0;
    for(int i = 0; i < attackDiceCount; i++) {
        attackPower += attackDice.roll();
    }

    return attackPower;
}

int Barbarian::defend(int attackPower) {
    int defensePower = 0;

    for(int i = 0; i < defenseDiceCount; i++) {
        defensePower += defenseDie.roll();
    }
    std::cout << defensePower << std::endl;

    defensePower += armor;

    if((attackPower - defensePower) > 0) {
        std::cout << "\tDamage Inflicted: " << (attackPower - defensePower) << std::endl;
        strength -= (attackPower - defensePower);
    } else {
        std::cout << "\tDamage Inflicted: 0" << std::endl;
    }

    return defensePower;
}
