/*********************************************************************
** Program name: Fantasy Combat
** Author: W. Aaron Morris
** Date:
** Description: Implementation of Barbarian
*********************************************************************/

#ifndef PROJECT3_BARBARIAN_H
#define PROJECT3_BARBARIAN_H

#include "Character.h"


class Barbarian : public Character {
public:
    Barbarian();

    /*!
     * The calculation of the attack of the Barbarian
     * @return
     */
    int attack() override;
    /*!
     * Defend an attack and calculate damage.
     * @param attack The attack the character is defending
     */
    void defense(int attackPower);

    int defend(int attackPower) override;
};


#endif //PROJECT3_BARBARIAN_H
