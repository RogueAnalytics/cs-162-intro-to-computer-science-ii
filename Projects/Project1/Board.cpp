/*********************************************************************
** Program name:
** Author:
** Date:
** Description: Implementation for Langston Board.
*********************************************************************/

#include "Board.h"
#include "Ant.h"
#include "Space.h"

#include <iostream>

void Board::printBoard() {
    for (int i = 0; i<rows; i++){
        for (int j = 0; j<columns; j++){
            std::cout << spaces[i][j]->getColor(true);
        }
        std::cout << std::endl;
    }
}

void Board::setBoard(int r, int c, int start_r, int start_c, Direction d) {
    rows = r;
    columns = c;
    spaces = new Space **[rows];

    for (int i = 0; i<rows; i++){
        spaces[i] = new Space *[columns];
        for (int j = 0; j<columns; j++){
            spaces[i][j] = new Space;
            spaces[i][j]->setSpace(j,i);
        }
    }

    //Set to zero base
    start_r = start_r - 1;
    start_c = start_c - 1;

    ant = new Ant;
    ant->setAnt(d, spaces[start_r][start_c]);
    spaces[start_r][start_c]->setAnt(ant);
}

Board::~Board() {
    for (int i = 0; i<rows; i++){
        for (int j = 0; j<columns; j++){
            delete spaces[i][j];
        }
        delete [] spaces[i];
    }
    delete [] spaces;
    delete ant;
}

void Board::runBoard() {
    int x;
    int y;
    x = ant->getLocation().x;
    y = ant->getLocation().y;

    //Move Ant in Direction it is pointing
    if (ant->getDirection()==NORTH) {
        //std::cout << "N" << std::endl;
        y--;
    }
    else if (ant->getDirection()==EAST){
        //std::cout << "E" << std::endl;
        x++;
    }
    else if (ant->getDirection()==SOUTH){
        //std::cout << "S" << std::endl;
        y++;
    }
    else if (ant->getDirection()==WEST){
        //std::cout << "W" << std::endl;
        x--;
    }
    if(y>=columns){
        y=0;
    } else if(y<0){
        y=columns-1;
    }

    if (x>=rows){
        x = 0;
    } else if(x<0){
        x=rows-1;
    }
    ant->moveAnt(spaces[y][x]);
    spaces[y][x]->setAnt(ant);

    printBoard();
}



