/*********************************************************************
** Program name:
** Author: W. Aaron Morris
** Date:
** Description: A space for the Langston simulation
*********************************************************************/

#ifndef PROJECT_1_ANT_HPP
#define PROJECT_1_ANT_HPP

#include "Space.h"


enum Direction {NORTH, EAST, SOUTH, WEST};

class Ant {

private:
    Direction direction;
    Space *ant_space;

public:
    void setAnt(Direction d, Space *space);
    void placeAnt(Space *space);
    void moveAnt(Space *space);
    void changeDirection();
    Position getLocation();
    Direction getDirection();
};


#endif //PROJECT_1_ANT_HPP
