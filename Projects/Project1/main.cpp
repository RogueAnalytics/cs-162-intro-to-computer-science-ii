#include <iostream>
#include <cstdlib>
#include "Ant.h"
#include "Space.h"
#include "Board.h"
#include "utility.h"

int main() {
    Board *board = new Board;
    int steps;
    int columns;
    int rows;
    int s_row;
    int s_column;
    int direction;
    Direction dir = NORTH;

    int start_app=1;

    utility::getMinMaxInt(start_app, 1, 2,
                          "1. Start Langton’s Ant simulation \n2. Quit",
                          "Not a valid input");

    while (start_app == 1){
        utility::getPositiveInt(steps,
                                "Enter Number of Steps",
                                "Not a valid input");

        utility::getPositiveInt(columns,
                                "Enter Number of Columns",
                                "Not a valid input");

        utility::getPositiveInt(rows,
                                "Enter Number of Rows: ",
                                "Not a valid input");

        utility::getMinMaxInt(s_column, 0, columns,
                              "Enter Starting Column (select 0 for a random number): ",
                              "Not a valid input");

        if (s_column==0){
            s_column = rand() % columns;
        }

        utility::getMinMaxInt(s_row, 0, rows,
                              "Enter Starting Row (select 0 for a random row): ",
                              "Not a valid input");

        if (s_row==0){
            s_row = rand() % rows;
        }

        utility::getMinMaxInt(direction, 1, 4,
                              "Enter Direction: \n 1. North \n 2. East \n 3. South \n 4. West",
                              "Not a valid input");

        if(direction==1){
            dir = NORTH;
        } else if (direction==2){
            dir = EAST;
        } else if (direction==3){
            dir = SOUTH;
        } else if (direction==4){
            dir = WEST;
        }


        board->setBoard(rows, columns, s_row, s_column, dir);
        std::cout << "Starting Board" << std::endl;
        for (int step = 0; step < steps; step++){
            std::cout << "Run: " << step << std::endl;
            board->runBoard();
       }

        utility::getMinMaxInt(start_app, 1, 2,
                              "1. Start Langton’s Ant simulation \n2. Quit",
                              "Not a valid input");
    }

    delete board;

    return 0;
}