/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT_1_SPACE_H
#define PROJECT_1_SPACE_H

#include <iostream>

class Ant;

struct Position{
    int x;
    int y;
};

class Space {

private:
    std::string color;
    Position position;
    Ant *ant;

public:
    Ant *getAnt();
    std::string getColor(bool ant);
    void changeColor();
    void setAnt(Ant *movedAnt);
    void removeAnt();
    Position getPosition();
    void setSpace(int x, int y);
};


#endif //PROJECT_1_SPACE_H
