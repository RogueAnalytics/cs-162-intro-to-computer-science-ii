/*******************************************************************************
** Author W. Aaron Morris
** Date: 6/25/18
** Description: A Class for Managing Everything that relates to the Ant.
*******************************************************************************/

#include "Ant.h"

void Ant::setAnt(Direction d, Space *space) {
    direction = d;
    placeAnt(space);
}

void Ant::moveAnt(Space *space) {
    ant_space->removeAnt();
    ant_space = space;
    changeDirection();
    ant_space->changeColor();
    ant_space->setAnt(space->getAnt());
}

void Ant::changeDirection() {
    // White Space ("_") -> Turn Right
    if (ant_space->getColor(false) == "_") {
        if (direction == NORTH) {
            direction = EAST;
        } else if (direction == EAST) {
            direction = SOUTH;
        } else if (direction == SOUTH) {
            direction = WEST;
        } else if (direction == WEST) {
            direction = NORTH;
        }
    }

        // Black Space ("#") -> Turn Left
    else if (ant_space->getColor(false) == "#") {
        if (direction == NORTH) {
            direction = WEST;
        } else if (direction == WEST) {
                direction = SOUTH;
        } else if (direction == SOUTH) {
            direction = EAST;
        } else if (direction == EAST) {
            direction = NORTH;
        }
    }
}

Direction Ant::getDirection(){
    return direction;
}

Position Ant::getLocation() {
    return ant_space->getPosition();
}

void Ant::placeAnt(Space *space) {
    ant_space = space;
    ant_space->changeColor();
}
