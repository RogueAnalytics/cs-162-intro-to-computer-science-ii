/*********************************************************************
** Program name: Zoo Tycoon
** Author: W. Aaron Morris
** Date: 7.17.28
** Description: This file implements the Zoo class.
*********************************************************************/


#ifndef PROJECT2_ZOO_H
#define PROJECT2_ZOO_H

#include "AnimalFactory.h"
#include "Animal.h"

class Zoo {
private:
    std::map<std::string, Animal**> animals;
    std::map<std::string, int> animalSlotCount;
    std::map<std::string, int> animalCount;
    std::map<int, int[4]> foodTypes;
    int food = 0;
    AnimalFactory *animalFactory;
    float money = 100000;
    int day=1;

public:
    Zoo();
    ~Zoo();
    float getMoney() const;
    int getDay();
    void cycleDay();
    void increaseAge();
    float feedAnimals();
    float event();
    float calculatePayoff();
    void purchaseAnimal();
    void addAnimalSlots(std::string name, Animal **animalSlots, int slotNumber);
    void extendAnimalSlots(std::string name);
    void addAnimal(std::string animalType, Animal *newAnimal);
    void removeAnimal(std::string animalType);
    void resetAnimalSlots(std::string animalType, int random);
    void HaveBaby(int listSize, std::string *animalList);
    void recordEvent(std::string message);
};


#endif //PROJECT2_ZOO_H
