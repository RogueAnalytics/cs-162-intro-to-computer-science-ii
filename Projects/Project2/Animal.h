/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT2_ANIMAL_H
#define PROJECT2_ANIMAL_H

#include <string>

const float FoodCost = 10.00;


class Animal {
private:
    //std::string animalType; /*!< Stores the type of Animal implemented */
    int age;  /*!< Age of the Animal */
    int numberOfBabies; /*!< Number of babies the Animal can generate */
    float cost; /*!< Cost of the Animal when purchased */
    float BaseFoodCost; /*!< Factor multiplier for the Food Cost for the Animal */
    float payoff; /*!< Factor multiplier for determining the daily payoff for the animal */
public:
    void setAge(int age);

//! Function to determine if the animal is an adult
    bool isAdult();

    //! Function to increase the age of an animal.
    void increaseAge();

    //! Function to set all Animal Variables.
    /*!
     * This sets all the variables in an animal.
     * \param babies an integer for how many babies an animal can produce
     * \param cost a float value for how much an animal costs to buy
     * \param foodCost a factor multiplier for how much the daily cost of food.
     * \param payoff a factor multiplier for the daily payoff of the animal.
     */
    virtual void setAnimal(int babies, float cost, float foodCost, float payoff);

    /** \addtogroup Getters and Setters
     * @{
     */
    int getAge() const;
    float getCost() const;
    void setCost(float cost);
    int getNumberOfBabies() const;
    float getBaseFoodCost() const;
    float getPayoff() const;
    /** @}*/
};



#endif //PROJECT2_ANIMAL_H
