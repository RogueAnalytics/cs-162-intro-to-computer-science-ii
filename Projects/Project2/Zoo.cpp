/*********************************************************************
** Program name: Zoo Tycoon
** Author: W. Aaron Morris
** Date: 7.17.28
** Description: This file implements the Zoo class.
*********************************************************************/


#include "Zoo.h"
#include "AnimalFactory.h"
#include "utility.h"
#include <iostream>
#include <fstream>
#include <string>

void Zoo::cycleDay() {
    int purchase = 0;
    if (day==1){
        do{
            purchaseAnimal();
            utility::getMinMaxInt(purchase, 1, 2,
                                  "Would you like to purchase an Animal? \n1. Yes \n2. No",
                                  "Invalid Choice");
        } while (purchase==1);
    }
    else{
        increaseAge();
        utility::getMinMaxInt(food, 1, 3, "Select the type of food?\n 1. Premium \n 2. Generic \n 3. Cheap ", "Not a valid choice.");
        food -= 1;

        money -= feedAnimals();
        money += event();
        money += calculatePayoff();
        do{
            utility::getMinMaxInt(purchase, 1, 2,
                                  "Would you like to purchase an Animal? \n 1.Yes \n 2.No",
                                  "Invalid Choice");
            if (purchase == 1){
                purchaseAnimal();
            }
        } while (purchase==1);
    }
    std::cout << "Results at the End of Day " << day << ":" << std::endl;
    std::cout << "Money:" << money << std::endl;
    for (auto const& x : animalCount){
        //animalList[index] = new std::string(x.first);
        std::cout << x.first << ": " << x.second << std::endl;
    }
    day++;
}

int Zoo::getDay() {
    return day;
}

void Zoo::increaseAge() {
    for (auto const& x : animals){
        for(int i = 0; i < animalCount[x.first]; i++){
            x.second[i]->increaseAge();
        }
    }
}

float Zoo::feedAnimals() {
    float foodCost;
    foodCost = 0;
    for (auto const& x : animals){
        for(int i = 0; i < animalCount[x.first]; i++){
            if ((food)==0){
                foodCost += (x.second[i]->getBaseFoodCost() * 2);
            } else if ((food) == 1){
                foodCost += (x.second[i]->getBaseFoodCost());
            } else if ((food) == 2){
                foodCost += (x.second[i]->getBaseFoodCost() / 2);
            }
        }
    }
    return foodCost;
}

void Zoo::recordEvent(std::string message){

    std::fstream file("Zoo Events.txt", std::ios::out | std::ios::app);

    if (!file.is_open()){
        std::cout << "Error writing event to file" << std::endl;
    } else {
        file << "Day: " + std::to_string(day) << std::endl;
        file << message << std::endl;
    }
    file.close();
}

float Zoo::event() {
    float rv = 0;
    int random = (rand() % 100) + 1;
    int listSize = animals.size();

    std::string *animalList = new std::string[listSize];
    int index = 0;

    //getting list of distinct animals
    for (auto const &x : animals) {
        //animalList[index] = new std::string(x.first);
        animalList[index] = x.first;
        index++;
    }

    if (random <= foodTypes[food][0]) {
        //Dead Animal
        random = (rand() % listSize);
        if (animalCount[animalList[random]] > 0) {
            removeAnimal(animalList[random]);
            recordEvent( "A " + animalList[random] + " was found dead.");
        }
        rv = 0;
    } else if (random <= foodTypes[food][1]) {
        //Boom in Attendance
        random = rand() % ((500 - 250) + 1) + 250;
        for (int i = 0; i < animalCount["Tiger"]; i++) {
            rv += random;
        }
        recordEvent("“Today is National Tiger Day! Tigers generate money today! You made: $"
                    + std::to_string(rv) + " extra dollars for each tiger you own!");
    } else if (random <= foodTypes[food][2]) {
        HaveBaby(listSize, animalList);
    }
    else if (random <= foodTypes[food][3]){
        recordEvent("Just another boring day....");
        rv = 0;
    }

    delete []animalList;
    return rv;
}

float Zoo::calculatePayoff() {
    float payoff;
    payoff = 0;
    for (auto const& x : animals){
        for(int i = 0; i < animalCount[x.first]; i++){
            payoff += x.second[i]->getPayoff();
        }
    }
    return payoff;
}

void Zoo::purchaseAnimal() {
    bool isError = true;
    Animal *purchasedAnimal;
    std::string selectedAnimal;
    std::cout << "What Animal would you like to purchase (See Choices below)? "
                 << "If the desire animal is not available, enter the name to create the animal"
                    << std::endl;
    do{
        //List animal options for user
        std::cout << "Tiger" << std::endl;
        std::cout << "Penguin" << std::endl;
        std::cout << "Turtle" << std::endl;
        if (animalFactory->getCustomAnimalBuilders().size()>0){
            for (auto const& x : animalFactory->getCustomAnimalBuilders()){
                std::cout << x.first << std::endl;
            }
        }
        std::cin >> selectedAnimal;
        purchasedAnimal = animalFactory->createAnimal(selectedAnimal, isError);

        //Give the User a chance to create an animal.
        if (isError){
            std::string newQuestion;
            int answer;
            utility::getMinMaxInt(answer, 1, 2,
                                  "Is this a new Animal? \n 1.Yes \n 2.No",
                                  "Invalid Choice");
            if (answer==1){
                animalFactory->addCustomAnimalBuilder();
                purchasedAnimal = animalFactory->createAnimal(selectedAnimal, isError);
            } else {
                std::cout << "Invalid Choice...Choose one of the following animals or create your own." << std::endl;
            }
        }
    } while (isError);

    if (day>1){
        purchasedAnimal->setAge(3);
    }

    money -= purchasedAnimal->getCost();
    addAnimal(selectedAnimal, purchasedAnimal);
}

void Zoo::addAnimal(std::string animalType, Animal *newAnimal){
    if(animalCount[animalType]==animalSlotCount[animalType]){
        extendAnimalSlots(animalType);
        std::cout << "The number of " << animalType << " has been expanded to " << animalSlotCount[animalType] << std::endl;
    }

    animals[animalType][animalCount[animalType]] = newAnimal;
    animalCount[animalType]++;
}

void Zoo::removeAnimal(std::string animalType) {
    int random = (rand() % (animalCount[animalType]));
    delete animals[animalType][random];
    resetAnimalSlots(animalType, random);
    animalCount[animalType]--;
}

void Zoo::resetAnimalSlots(std::string animalType, int random) {
    for(int i = random; i < animalCount[animalType]; i++) {
        animals[animalType][i] =  animals[animalType][i+1];
    }
    if (random != (animalCount[animalType]-1)){
        delete animals[animalType][(animalCount[animalType]-1)];
    }
}

void Zoo::addAnimalSlots(std::string name, Animal **animalSlots,
                         int slotNumber){
    animals.insert(std::pair<std::string, Animal**>(name, animalSlots));
    animalSlotCount.insert(std::pair<std::string, int>(name, slotNumber));
}

void Zoo::extendAnimalSlots(std::string name){
    int slotSize;
    slotSize = animalSlotCount[name] * 2;
    Animal **newSlots =  new Animal*[slotSize];
    for(int i = 0; i < animalSlotCount[name]; i++) {
        newSlots[i] =  animals[name][i];
    }
    animals[name] = newSlots;
    animalSlotCount[name] = slotSize;
}

Zoo::Zoo() {
    animalFactory = new AnimalFactory();
    int intialSlots = 10;

    // Setting Food Event Weights
    //Premium Food
    foodTypes[0][0] = 12;
    foodTypes[0][1] = 37;
    foodTypes[0][2] = 62;
    foodTypes[0][3] = 100;

    //Generic
    foodTypes[1][0] = 25;
    foodTypes[1][1] = 50;
    foodTypes[1][2] = 75;
    foodTypes[1][3] = 100;

    //Cheap
    foodTypes[2][0] = 50;
    foodTypes[2][1] = 66;
    foodTypes[2][2] = 82;
    foodTypes[2][3] = 100;

    addAnimalSlots("Tiger", new Animal *[intialSlots], intialSlots);
    addAnimalSlots("Penguin", new Animal *[intialSlots], intialSlots);
    addAnimalSlots("Turtle", new Animal *[intialSlots], intialSlots);
}

Zoo::~Zoo() {
    delete animalFactory;
    for (auto const& x : animals){
        for (int k = 0 ; k < animalCount[x.first]; k++){
            delete x.second[k];
        }
        delete [] x.second;
    }
}

void Zoo::HaveBaby(int listSize, std::string *animalList){
    // Baby
    int random;
    bool hadBaby = false;
    while (!hadBaby){
        Animal *baby;
        random = (rand() % listSize);
        if (animalCount[animalList[random]]>0){
            for (int j = 0; j < (animalCount[animalList[random]]); j++){
                bool isError;

                if (animals[animalList[random]][j]->getAge() > 3){
                    int babies = animals[animalList[random]][j]->getNumberOfBabies();
                    for (int bb=0; bb < babies; bb++){
                        baby = animalFactory->createAnimal(animalList[random], isError);
                        baby->setAge(0);
                        addAnimal(animalList[random], baby);
                    }
                    recordEvent("A " + animalList[random] + " gave birth");
                    hadBaby = true;
                    break;
                }else{
                    utility::removeArrayItem(animalList, listSize, j);
                }
                if (listSize==1){
                    recordEvent("It is birthing season, but no animals to give birth");
                    hadBaby=true;
                    break;
                }
            }
        }

        if (listSize<=1){
            recordEvent("It is birthing season, but no animals to give birth");
            hadBaby=true;
        }
    }
}

float Zoo::getMoney() const {
    return money;
}
