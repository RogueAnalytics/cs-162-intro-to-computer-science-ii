/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT2_CUSTOMANIMAL_H
#define PROJECT2_CUSTOMANIMAL_H

#include "Animal.h"

class CustomAnimal: public Animal {
public:
    CustomAnimal(int babies, int cost,
                 float foodCost, float payoff);
};


#endif //PROJECT2_CUSTOMANIMAL_H
