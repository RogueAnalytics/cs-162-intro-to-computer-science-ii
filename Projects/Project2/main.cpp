#include <iostream>
#include "Zoo.h"
#include "utility.h"

int main() {
    int playGame;
    Zoo *zoo = new Zoo();
    utility::getMinMaxInt(playGame, 1, 2,
                          "Do you want to play Zoo Tycoon? \n1.) Play Game \n2.) Exit",
                          "Invalid Choice");

    while (playGame==1){
        zoo->cycleDay();
        if (zoo->getMoney() >= 0){
            utility::getMinMaxInt(playGame, 1, 2,
                                  "Do you want to continue playing? \n1.) Yes \n2.) No",
                                  "Invalid Choice");
        } else {
            std::cout << "Your zoo went bankrupt. \nGame Over...........";
            playGame = 0;
        }


    }

    delete zoo;

    return 0;
}