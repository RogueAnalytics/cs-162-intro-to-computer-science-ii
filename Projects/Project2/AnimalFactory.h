/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT2_ANIMALFACTORY_H
#define PROJECT2_ANIMALFACTORY_H

#include <map>
#include <string>
#include "NewAnimalBuilder.h"
#include "Animal.h"
#include "Tiger.h"
#include "Penguin.h"
#include "Turtle.h"


class AnimalFactory {
private:
    std::map<std::string, NewAnimalBuilder*> customAnimalBuilders;
public:

    //! Function to instatiate an Animal object
    /*!
     * This sets all the variables in an animal.
     * \param description Name of the Type of Animal being created.
     * \return Animal
     */
    Animal *createAnimal(const std::string &description, bool &error);

    //! Function to store all custom animal builders
    /*!
     * This sets all the variables in an animal.
     * \param name an string for the type of animal the builder is.
     * \param animalBuilder an animal builder class
     */
    void insertCustomAnimal( std::string name,
                             NewAnimalBuilder *animalBuilder);

    //! A simple getter for the list of custom Animals and their Builders
    /*!
     * A dictionary of Animal that have pointers to their builders.
     */
    const std::map<std::string, NewAnimalBuilder*> &getCustomAnimalBuilders() const;

    //! Create an Animal Builder and inserts it into Animal Dictionary
    /*!
     * This prompts the user for all needed variables to create a new animal.
     */
    void addCustomAnimalBuilder();

    virtual ~AnimalFactory();
};


#endif //PROJECT2_ANIMALFACTORY_H
