/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "AnimalFactory.h"
#include "utility.h"


Animal *AnimalFactory::createAnimal(const std::string &description, bool &error) {
    Animal *createdAnimal;

    if(description == "Tiger") {
        error = 0;
        return new Tiger;
    }
    else if(description == "Penguin") {
        error = 0;
        return new Penguin;
    }
    else if(description == "Turtle") {
        error = 0;
        return new Turtle;
    }
    else if (customAnimalBuilders.find(description)!= customAnimalBuilders.end()) {
        error = 0;
        return customAnimalBuilders[description]->createAnimal();
    }
    else {
         error = 1;
        return nullptr;
    }
}

const std::map<std::string, NewAnimalBuilder *>
        &AnimalFactory::getCustomAnimalBuilders() const
{
    return customAnimalBuilders;
}


void AnimalFactory::insertCustomAnimal( std::string name,
                                        NewAnimalBuilder *animalBuilder)
{
    customAnimalBuilders.insert(
            std::pair<std::string, NewAnimalBuilder* >(name, animalBuilder)
                    );
}

void
AnimalFactory::addCustomAnimalBuilder() {
    std::string name;
    int babies;
    float cost;
    float foodCost;
    float payoff;

    std::cout << "Type of Animal being Added: ";
    std::cin >> name;

    NewAnimalBuilder *ab = new NewAnimalBuilder(babies, cost, foodCost, payoff);

    insertCustomAnimal(name, ab);
}

AnimalFactory::~AnimalFactory() {
    for (auto const& x : customAnimalBuilders){
        delete x.second;
        customAnimalBuilders.erase(x.first);
    }
}
