/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT2_PENGUIN_H
#define PROJECT2_PENGUIN_H


#include "Animal.h"

class Penguin: public Animal {
public:
    //! Default Penguin Constructor
    /*!
     * Inheriting the basic Animal class with Penguin
     * configuration and functions.
    */
    Penguin();
};


#endif //PROJECT2_PENGUIN_H
