/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "NewAnimalBuilder.h"
#include "CustomAnimal.h"
#include "utility.h"

CustomAnimal *NewAnimalBuilder::createAnimal(){
    return new CustomAnimal(babies, cost, foodCost, payoff);
}

NewAnimalBuilder::NewAnimalBuilder(int babies, float cost,
                                   float foodCost, float payoff){
    utility::getPositiveInt(babies,
                            "Enter Number of Babies the Animal can have: ",
                            "Invalid Choice");

    utility::getFloat(cost,
                      "Enter the Cost of Animal: ",
                      "Invalid Choice");

    utility::getFloat(foodCost,
                      "Enter the Factor Multiplier for the Animal's Food: ",
                      "Invalid Choice");

    utility::getFloat(payoff,
                      "Enter the Factor Multiplier for the Animal's Daily Payoff: ",
                      "Invalid Choice");

    //Animal::animalType = type;
    NewAnimalBuilder::babies = babies;
    NewAnimalBuilder::cost = cost;
    NewAnimalBuilder::foodCost = foodCost;
    NewAnimalBuilder::payoff = payoff;
}
