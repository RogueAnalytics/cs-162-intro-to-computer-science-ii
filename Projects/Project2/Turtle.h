/*********************************************************************
** Program name: Zoo Tycoon
** Author: W. Aaron Morris
** Date: 7.17.28
** Description: This file implements the Turtle class.
*********************************************************************/


#ifndef PROJECT2_TURTLE_H
#define PROJECT2_TURTLE_H


#include "Animal.h"

class Turtle : public Animal{
/*!
 * Class for implementing a turtle class in on the standard Animal
*/
public:
    Turtle();

};


#endif //PROJECT2_TURTLE_H
