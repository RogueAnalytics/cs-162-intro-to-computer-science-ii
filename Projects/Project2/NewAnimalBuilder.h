/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef PROJECT2_NEWANIMALFACTORY_H
#define PROJECT2_NEWANIMALFACTORY_H

#include "CustomAnimal.h"

class NewAnimalBuilder {
public:
    CustomAnimal *createAnimal();
    NewAnimalBuilder(int babies, float cost, float foodCost, float payoff);

private:
    int babies;
    float cost;
    float foodCost;
    float payoff;
};


#endif //PROJECT2_NEWANIMALFACTORY_H
