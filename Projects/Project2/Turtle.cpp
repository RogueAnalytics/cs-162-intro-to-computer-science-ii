/*********************************************************************
** Program name: Zoo Tycoon
** Author: W. Aaron Morris
** Date: 7.17.28
** Description: This file implements the Turtle class.
*********************************************************************/

#include "Turtle.h"

Turtle::Turtle() {
    /*!
     * Constructor loading the basic Animal class with the Turtle configuration
     *
     */
        Animal::setAnimal(10, 100, .5, .05);
}
