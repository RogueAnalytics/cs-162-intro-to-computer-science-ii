/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Animal.h"
#include <string>

void Animal::setAnimal(int babies, float cost, float foodCost, float payoff){
    //Animal::animalType = type;
    Animal::age = 1;
    Animal::numberOfBabies = babies;
    Animal::cost = cost;
    Animal::BaseFoodCost = foodCost;
    Animal::payoff = payoff;
}

int Animal::getAge() const {
    return age;
}

bool Animal::isAdult(){
    bool rv = 0;
    if (age >= 3){
        rv = 1;
    }
    return rv;
}

void Animal::increaseAge(){
    Animal::age++;
}

float Animal::getCost() const {
    return cost;
}

void Animal::setCost(float cost) {
    Animal::cost = cost;
}

int Animal::getNumberOfBabies() const {
    return numberOfBabies;
}

float Animal::getBaseFoodCost() const {
    return BaseFoodCost*FoodCost;
}

float Animal::getPayoff() const {
    return (payoff*cost);
}

void Animal::setAge(int age) {
    Animal::age = age;
}

