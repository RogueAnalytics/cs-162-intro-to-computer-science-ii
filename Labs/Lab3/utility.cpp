/*********************************************************************
** Program name: Utility Functions
** Author: W. Aaron Morris
** Date: 06/27/18
** Description: This file deals with the implementation of all the utility
 * functions that are going to be used during the course of CS162
*********************************************************************/

#include <iostream>
#include "utility.h"


namespace utility {
    void getInt(int &num, std::string q, std::string i) {
        bool input_error;
        int input;

        std::cin >> input;

        do{
            input_error = false;
            if (std::cin.fail()){
                input_error = true;
            }

            std::cin.clear();
            std::cin.ignore();
            std::cout << i << ": " << std::endl;
        } while (input_error);
        num = input;
    }

    void getPositiveInt(int &num, std::string q, std::string i) {
        int input;
        bool input_error;

        do {
            input_error = false;
            std::cout << q << std::endl;
            std::cin >> input;

            // handle the case where the input does not match variable type
            if (std::cin.fail()) {
                input_error = true;
                std::cout << i << std::endl;
            }
            if (input <= 0) {
                input_error = true;
                std::cout << i << std::endl;
            }
            std::cin.clear();
            std::cin.ignore();
        } while (input_error);

        // if input is accepted, assign x to the target
        num = input;
    }

    void getMinMaxInt(int &num, int min, int max, std::string q, std::string i) {
        int input;
        bool input_error;

        do {
            input_error = false;
            std::cout << q << std::endl;
            std::cin >> input;

            // handle the case where the input does not match variable type
            if (std::cin.fail()) {
                input_error = true;
                std::cout << i << std::endl;
                std::cout << min;
            }
            if (input < min || input>max) {
                std::cout << min;
                input_error = true;
                std::cout << i << std::endl;
            }
            std::cin.clear();
            std::cin.ignore();
        } while (input_error);

        // if input is accepted, assign x to the target
        num = input;
    }
}

