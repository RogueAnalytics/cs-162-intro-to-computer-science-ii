/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron MOrris
** Date:
** Description: Implementation of the Loaded Dice class. This class leverages
 *              inheritance from the Dice class.
*********************************************************************/

#ifndef LAB_3_LOADEDDICE_H
#define LAB_3_LOADEDDICE_H

#include "Dice.h"
#include <iostream>
#include <cstdlib>


class LoadedDice : public Dice {
public:
    int rollDice();
};


#endif //LAB_3_LOADEDDICE_H
