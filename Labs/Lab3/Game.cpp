/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron Morris
** Date:
** Description: implementation of how to play the game of War
*********************************************************************/

#include <iostream>
#include <iomanip>
#include <string>
#include "utility.h"
#include "Game.h"
#include "Dice.h"
#include "loadedDice.h"

void Game::playGame() {
    player[0].score = 0;
    player[1].score = 0;
    int result[2];
    std::cout << std::setw(10) << "Round";
    std::cout << std::setw(10) << "Player";
    std::cout << std::setw(15) << "Dice Type";
    std::cout << std::setw(15) << "Round Result";
    std::cout << std::setw(10) << "Score" << std::endl;
    for (int i = 0; i < rounds; i++){
        result[0] = player[0].die->rollDice();
        result[1] = player[1].die->rollDice();

        if (result[0] > result[1]){
            player[0].score++;
        } else if (result[1] > result[0]){
            player[1].score++;
        }

        for (int p =0; p < 2; p++){
            std::cout << std::setw(10) << i+1;
            std::cout << std::setw(10) << p+1;
            std::cout << std::setw(15) << player[p].DiceType;
            std::cout << std::setw(15) << result[p];
            std::cout << std::setw(10) << player[p].score << std::endl;
        }


    }
    if (player[0].score > player[1].score){
        std::cout << "PLAYER 1 WINS!!!!!!!!" << std::endl;
    }
    else if (player[0].score < player[1].score){
        std::cout << "PLAYER 2 WINS!!!!!!!!" << std::endl;
    }
    else{
        std::cout << "DRAW!!!"<< std::endl;
    }

}

int Game::gameMenu() {
    int start;
    int diceType;
    int side;
    utility::getMinMaxInt(start, 1, 2, "1. Start Game \n2. End Game", "Not a valid choice.");

    while (start==1){
        std::string message;

        utility::getPositiveInt(rounds, "How many rounds should be played?", "Not a valid choice");

        for (int i = 0; i < 2; i++){

            message = "Select the Type of Dice for Player " + std::to_string(i+1) + "\n\t1. Normal Dice \n\t2. Loaded Dice";

            utility::getMinMaxInt(diceType, 1, 2, message, "Not a valid choice");

            message = "Type the Number of Sides for Player" + std::to_string(i+1) + ": ";

            utility::getPositiveInt(side, message, "Not a valid choice");

            if (diceType ==1){
                player[i].DiceType = "Normal Dice";
                player[i].die = new Dice();
                player[i].die->setSides(side);
            }
            else if (diceType==2){
                player[i].DiceType = "Loaded Dice";
                player[i].die = new LoadedDice();
                player[i].die->setSides(side);
            }

        }
        playGame();
        deleteDice();

        std::cout << "----------------------------------------------" << std::endl;

        utility::getMinMaxInt(start, 1, 2, "1. Start Game \n2. End Game", "Not a valid choice.");
    }
    return 0;
}

void Game::deleteDice(){
    for (int p =0; p < 2; p++){
        delete player[p].die;
    }
}

Game::~Game() {
    //pointer needs to be freed after every round.
    //deleteDice();
}
