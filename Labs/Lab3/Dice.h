/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron Morris
** Date:
** Description: implementation of the Dice class
*********************************************************************/

#ifndef LAB_3_DICE_H
#define LAB_3_DICE_H


class Dice {

protected:
    int N;

public:
    virtual int rollDice();
    virtual void setSides(int sides);
    int getSides();
};


#endif //LAB_3_DICE_H
