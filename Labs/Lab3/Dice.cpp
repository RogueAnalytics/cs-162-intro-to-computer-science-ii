/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron Morris
** Date:
** Description: implementation of the Dice class
*********************************************************************/

#include "Dice.h"

#include <cstdlib>

int Dice::rollDice() {
    return (std::rand() % N) + 1;
}

int Dice::getSides() {
    return N;
}

void Dice::setSides(int N) {
    Dice::N = N;
}
