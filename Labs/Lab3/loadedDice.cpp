/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron MOrris
** Date:
** Description: Implementation of the Loaded Dice class. This class leverages
 *              inheritance from the Dice class.
*********************************************************************/

#include "loadedDice.h"
#include <cstdlib>
#include <iostream>


int LoadedDice::rollDice() {
    int roll = Dice::rollDice();
    roll = roll + 5;
    if (roll > Dice::getSides()){
        roll = Dice::getSides();

    }
    return roll;
}
