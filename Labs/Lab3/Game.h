/*********************************************************************
** Program name: The Game of War
** Author: W. Aaron Morris
** Date:
** Description: implementation of how to play the game of War
*********************************************************************/

#ifndef LAB_3_GAME_H
#define LAB_3_GAME_H

#include "Dice.h"

//A player struct for storing all the information regarding a player
// To-Do: This should be a class and score should be abstracted away from the
//          game class.
struct Player{
    int score;
    Dice *die;
    std::string DiceType;
};

//The Game class
class Game {
private:
    Player player[2];
    int rounds;

public:
    void playGame();
    int gameMenu();
    void deleteDice();

    virtual ~Game();
};


#endif //LAB_3_GAME_H
