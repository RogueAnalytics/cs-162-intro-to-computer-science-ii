/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_BUILDING_H
#define LAB4_BUILDING_H

#include <string>


class Building {
private:
    std::string name;
    int size;
    std::string address;
public:
    Building(std::string n, std::string a, int s);
    std::string writeData();

    void print_information();

};


#endif //LAB4_BUILDING_H
