/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include <iostream>
#include "Student.h"

void Student::do_work() {
    //PERSON_NAME did X hours of homework.
    std::cout << name << " did " << std::to_string((rand() % 24)) << " hours of homework." << std::endl;
}

void Student::print_information() {
    //Maybe make this tabular.
    std::cout << "----------------------------" << std::endl;
    std::cout << "Person Role: Student" << std::endl;
    std::cout << "Name: " << name << std::endl;
    std::cout << "Age: " << age << std::endl;
    std::cout << "GPA: " << GPA << std::endl;
}

std::string Student::write_data() {
    return "Student," + name + "," + std::to_string(age) + "," + std::to_string(GPA);
}


Student::Student(std::string n, int a, float g) {
    name = n;
    age = a;
    GPA = g;
}
