/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_PERSONFACTORY_H
#define LAB4_PERSONFACTORY_H

#include <string>
#include <iostream>
#include "Person.h"
#include "Student.h"
#include "Instructor.h"

Person *PersonFactory(bool &hasError, std::string personType,
                      std::string name, int age, float performance);

#endif //LAB4_PERSONFACTORY_H
