/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Instructor.h"
#include <iostream>


void Instructor::do_work() {
    //PERSON_NAME graded papers for X hours.
    std::cout << name << " graded papers for " << std::to_string((rand() % 24)) << " hours." << std::endl;
}

void Instructor::print_information() {
    std::cout << "----------------------------" << std::endl;
    std::cout << "Person Role: Instructor" << std::endl;
    std::cout << "Name: " << name << std::endl;
    std::cout << "Age: " << age << std::endl;
    std::cout << "Rating: " << rating << std::endl;
}

Instructor::Instructor(std::string n, int a, float r) {
    name = n;
    age = a;
    rating = r;
}

std::string Instructor::write_data() {
    return "Instructor," + name + "," + std::to_string(age) + "," + std::to_string(rating);
}
