/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "University.h"
#include "utility.h"
#include "PersonFactory.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

bool University::printMenu() {
    bool rv = false;
    int selection;
    utility::getMinMaxInt(selection, 1, 6,
                          "Please select 1 of the following:\n"
                          " 1.Add a buildings\n"
                          " 2.Add a person\n"
                          " 3.Prints information about all the buildings\n"
                          " 4.Prints information of everybody at the university\n"
                          " 5.Choose a person to do work\n"
                          " 6.Exit the program"
                          , "Invalid Choice");

    if (selection==1){
        inputBuilding();
    } else if (selection==2){
        inputPerson();
    } else if (selection==3){
        printBuildings();
    } else if (selection==4){
        printPeople();
    } else if (selection==5){
        workPerson();
    } else if (selection==6){
        writeBuildings();
        writePeople();
        rv = true;
    }

    return rv;

}

void University::printBuildings() {
    for(int i = 0; i < BuildingCount; i++){
        buildings[i]->print_information();
    }

}

void University::printPeople() {
    for (auto x : people){
        x.second->print_information();
    }
}

void University::addBuilding(Building *building) {
    if(BuildingCount>=BuildingSlots){
        extendBuildingSlots();
    }

    buildings[BuildingCount] = building;
    BuildingCount++;
}

void University::inputBuilding() {
    std::string n;
    std::string a;
    int size;

    std::cout << "Type Name of Building" << std::endl;
    std::cin >> n;

    std::cout << "Type in the address of "<< n << "?" << std::endl;
    std::cin >> a;

    std::cout << "How many square foot is " << n << "?" << std::endl;
    std::cin >> size;

    addBuilding(new Building(n, a, size));
}

void University::extendBuildingSlots(){
    int slotSize;
    slotSize = BuildingSlots * 2;
    Building **newSlots =  new Building*[slotSize];
    for(int i = 0; i < BuildingCount; i++) {
        newSlots[i] =  buildings[i];
    }
    buildings= newSlots;
    BuildingSlots = slotSize;
}

void University::addPerson(Person *person) {
    /**********
    if(PeopleCount>=PeopleSlots){
        extendPeopleSlots();
    }
     *************************/

    people[person->getName()] = person;
    PeopleCount++;
}

void University::inputPerson() {
    std::string ptype;
    std::string name;
    int age;
    float performance;
    int size;
    bool wrongType = true;

    std::cout << "Type Name of Person?" << std::endl;
    std::cin >> name;

    std::cout << "What is "<< name << " age?" << std::endl;
    std::cin >> age;

    while (wrongType){
        std::cout << "Is this a Instructor or Student?" << std::endl;
        std::cin >> ptype;

        if (ptype == "Student"){
            wrongType = false;
            utility::getMinMaxFloat(performance, 0, 4,
                                    "What is " + name + "'s GPA?" ,
                                    "Invalid Choice");
        } else if (ptype=="Instructor"){
            wrongType = false;
            utility::getMinMaxFloat(performance, 0, 5,
                                    "What is " + name + "'s rating?",
                                    "Invalid Choice");
        }
    }

    addPerson(PersonFactory(wrongType, ptype, name, age, performance));

}

/**************************************
 * Switched to using a map to easily get names to help
 * user select individual to work.
 *
void University::extendPeopleSlots() {
    int slotSize;
    slotSize = PeopleSlots * 2;
    Person **newSlots =  new Person*[slotSize];
    for(int i = 0; i < PeopleCount; i++) {
        newSlots[i] =  people[i];
    }

    people = newSlots;
    PeopleSlots = slotSize;
}
 **************************************/

void University::workPerson() {
    std::string selected;
    bool validPerson = false;
    std::cout << "Please select one of the following people to work:" << std::endl;
    for (auto x : people){
        std::cout << x.first << std::endl;
    }
    std::cin >> selected;
    if ( people.find(selected) == people.end() ) {
        validPerson = false;
    } else {
        validPerson = true;
        people.find(selected)->second->do_work();
    }
}

void University::loadPeople() {
    bool isError;
    std::ifstream peopleFile("people.csv");
    std::string record;
    std::string feature;
    std::string features[4];
    int index;

    while (std::getline(peopleFile, record))
    {
        index = 0;

        std::istringstream ss(record);

        while(std::getline(ss, feature, ',')){
            features[index] = feature;
            index++;
        }

        addPerson(PersonFactory(isError, features[0],
                                features[1], std::stoi(features[2]),
                                std::stof(features[3])));

    }
}

void University::loadBuildings() {
    std::ifstream buildingFile("buildings.csv");
    std::string record;
    std::string feature;
    std::string features[3];
    int index;

    while (std::getline(buildingFile, record))
    {
        index = 0;

        std::istringstream ss(record);

        while(std::getline(ss, feature, ',')){
            features[index] = feature;
            index++;
        }
        addBuilding(new Building(features[0], features[1], std::stoi(features[2])));

    }
}

University::~University() {
    for (int i = 0; i < BuildingCount; i++){
        delete buildings[i];
    }

    delete [] buildings;

    for (auto x: people){
        delete x.second;
    }

}

void University::writePeople() {
    std::ofstream peopleFile("people.csv");
    for (auto x : people){
        peopleFile << x.second->write_data() << std::endl;
    }

}

void University::writeBuildings() {
    std::ofstream buildingFile("buildings.csv");

    for (int i = 0; i < BuildingCount; i++){
        buildingFile << buildings[i]->writeData() << std::endl;
    }

}

University::University() {
    loadBuildings();
    loadPeople();
}


