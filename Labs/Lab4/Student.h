/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_STUDENT_H
#define LAB4_STUDENT_H


#include "Person.h"

class Student : public Person {
private:
    float GPA;
public:
    Student(std::string n, int a, float g);
    std::string write_data();
    void do_work();
    void print_information();
};


#endif //LAB4_STUDENT_H
