#include <iostream>
#include "University.h"

int main() {
    bool exit;
    University *osu = new University();

    do {
        exit = osu->printMenu();
    } while (!exit);

    delete osu;

    return 1;
}