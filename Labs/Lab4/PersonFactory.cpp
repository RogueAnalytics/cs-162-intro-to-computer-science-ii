/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "PersonFactory.h"

Person *PersonFactory(bool &hasError, std::string personType,
                      std::string name, int age, float performance){
    Person *rv;
    if (personType == "Student"){
        hasError = false;
        rv = new Student(name, age, performance);
    } else if (personType == "Instructor"){
        hasError = false;
        rv = new Instructor(name, age, performance);
    } else {
        hasError = true;
        rv = nullptr;
    }

    return rv;
}
