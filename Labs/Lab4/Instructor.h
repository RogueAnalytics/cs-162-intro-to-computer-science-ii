/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_INSTRUCTOR_H
#define LAB4_INSTRUCTOR_H


#include "Person.h"

class Instructor : public Person{
private:
    float rating;

public:
    Instructor(std::string n, int a, float r);
    std::string write_data();
    void do_work();
    void print_information();
};


#endif //LAB4_INSTRUCTOR_H
