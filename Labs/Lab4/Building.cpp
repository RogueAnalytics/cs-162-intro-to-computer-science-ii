/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Building.h"

#include <iostream>

void Building::print_information() {
    //Maybe make this tabular.
    std::cout << "----------------------------" << std::endl;
    std::cout << "Name: " << name << std::endl;
    std::cout << "Address: " << address << std::endl;
    std::cout << "Size: " << size << std::endl;

}

Building::Building(std::string n, std::string a, int s){
    name = n;
    address = a;
    size = s;
}

std::string Building::writeData() {
    return name + "," + address + "," + std::to_string(size);
}
