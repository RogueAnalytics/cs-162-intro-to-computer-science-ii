/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_PERSON_H
#define LAB4_PERSON_H

#include <string>


class Person {
protected:
    std::string name;
    int age;
public:
    const std::string getName();
    virtual std::string write_data()=0;
    virtual void do_work()=0;
    virtual void print_information()=0;
    virtual ~Person();
};


#endif //LAB4_PERSON_H
