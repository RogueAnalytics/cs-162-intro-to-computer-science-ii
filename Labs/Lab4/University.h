/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB4_UNIVERSITY_H
#define LAB4_UNIVERSITY_H

#include "Building.h"
#include <map>
#include "Person.h"


class University {
private:
    std::string name = "Oregon State University";
    int BuildingCount=0;
    int BuildingSlots=0;
    int PeopleCount=0;
    int PeopleSlots=0;
    Building **buildings;
    //Person **people;
    std::map<std::string, Person *> people;

public:
    University();

    bool printMenu();
    void printBuildings();
    void printPeople();
    void addBuilding(Building *building);
    void inputBuilding();
    void extendBuildingSlots();
    void addPerson(Person *person);
    void inputPerson();
    //void extendPeopleSlots();
    void workPerson();
    void writePeople();
    void loadPeople();
    void writeBuildings();
    void loadBuildings();
    ~University();
};


#endif //LAB4_UNIVERSITY_H
