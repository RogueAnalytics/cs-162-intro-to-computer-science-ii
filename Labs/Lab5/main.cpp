#include <iostream>
#include "utility.h"
#include "RecursiveFunctions.h"

int main() {
    int choice;


    do{
        utility::getMinMaxInt(choice, 1, 4,
                              "PLease select 1 of the following: "
                              "\n1.) Reverse a string "
                              "\n2.) Summation of Numbers"
                              "\n3.Calculate Trianglular Number"
                              "\n4.Quit",
                              "Invalid selection");

        if (choice==1){
            std::string rev;
            std::cout << "Please enter a string to reverse:" << std::endl;
            std::cin >> rev;
            reverseString(rev);
            std::cout << std::endl;
        } else if (choice == 2){
            int numberOfInt;
            utility::getPositiveInt(numberOfInt, "How many integers to you want to sum?", "Invalid Choice");
            int numbers[numberOfInt];
            for (int i = 0; i<numberOfInt; i++){
                std::cout << "Enter an integer (" << (numberOfInt-i) << " remaining integers):" << std::endl;
                std::cin >> numbers[i];
            }
            std::cout << "The sum total of your integers is ";
            std::cout << summation(numbers, numberOfInt) << std::endl;
        } else if (choice == 3){
            int tNumber;
            utility::getPositiveInt(tNumber, "What number would you like to calculate the Triangular number?", "Invalid Choice");
            std::cout << "The triangle number of your number is ";
            std::cout << triangular(tNumber) << std::endl;
        }
    }
    while( choice!=4);




}