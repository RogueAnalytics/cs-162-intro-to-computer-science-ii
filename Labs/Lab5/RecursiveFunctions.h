/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB5_RECURSIVEFILES_H
#define LAB5_RECURSIVEFILES_H

#include <string>

/*!
 * A function that recursively prints a string in reverse.
 * \param str the string to reverse
 */
void reverseString(std::string str);

/*!
 * A function to recursively sum an array
 * @param array
 * @param elements
 *
 * @return Sum of the array.
 */
int summation(int array[], int elements);

/*!
 * A function that recursively calculates the triangular number of an integer.
 * @param n The integer to caluclate the triangle number
 */
int triangular(int n);


#endif //LAB5_RECURSIVEFILES_H
