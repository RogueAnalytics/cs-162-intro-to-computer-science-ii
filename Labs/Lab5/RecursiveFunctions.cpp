/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "RecursiveFunctions.h"

#include <iostream>

void reverseString(std::string str) {
    int chars = str.size();

    if(chars == 1)
        std::cout << str << std::endl;
    else
    {
        std::cout << str[chars - 1];
        reverseString(str.substr(0, chars - 1));
    }

}

int summation(int *array, int elements) {
    if (elements <= 0){
        return 0;
    }
    return (array[elements - 1] + summation(array , elements-1));

}


int triangular(int n) {
    if (n==0){
        return 0;
    }

    return (n + triangular(n-1));
}
