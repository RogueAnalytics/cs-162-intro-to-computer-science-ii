/*********************************************************************
** Program name: Queue and Stack
** Author: W. Aaron Morris
** Date:
** Description: Buffer implementation
*********************************************************************/

#include "Buffer.h"
#include "utility.h"
#include <iostream>

int Buffer::generateNumber() {
    return (rand() % 1000) + 1;
}

void Buffer::addTail() {
    if ((((rand()) % 100)+1) <= addTailProb){
        q.push(generateNumber());
    }
}

void Buffer::removeHead() {
    if ((((rand()) % 100)+1) <= removeHeadProb){
        q.pop();
    }
}

void Buffer::simulate() {
    for (int i = 0; i < round; i++){
        std::cout << "------Round " << i+1 << "------" << std::endl;
        addTail();
        removeHead();
        output(i+1);
    }
}

void Buffer::output(int currentRound) {
    int numberInQueue = q.size();
    if (!q.empty()){
        for( int i = 0; i < numberInQueue; i++ ){
            std::cout << q.front() << " ";
            if (numberInQueue > 0){
                q.push(q.front());
                q.pop();
            }
        }
        std::cout << std::endl;
        std::cout << "Number of Items in Buffer: " << q.size() << std::endl;
        averageLength = (((averageLength * (currentRound-1)) + q.size())/currentRound);
        std::cout << "Average Number of Items in the Buffer: " << averageLength << std::endl;
    } else{
        std::cout << "No items in the buffer" << std::endl;
        std::cout << "Number of Items in Buffer: " << 0 << std::endl;
        averageLength = (((averageLength * (currentRound-1)) + 0)/currentRound);
        std::cout << "Average Number of Items in the Buffer: " << averageLength << std::endl;
    }

}

Buffer::Buffer() {
    int r;
    int t;
    int h;

    utility::getMinMaxInt(r, 1, INT_MAX, "Enter Number of Rounds to Simulate the Buffer", "Not a Valid Choice");
    utility::getMinMaxInt(t, 1, 100, "Enter Probability (in integer format) of Generating a New Number", "Not a Valid Choice");
    utility::getMinMaxInt(h, 1, 100, "Enter Probability (in integer format) of Removing a Number", "Not a Valid Choice");

    round = r;
    removeHeadProb = h;
    addTailProb = t;

}

