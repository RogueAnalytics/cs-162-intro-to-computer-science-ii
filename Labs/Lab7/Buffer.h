/*********************************************************************
** Program name: Queue and Stack
** Author: W. Aaron Morris
** Date:
** Description: Buffer Header
*********************************************************************/

#ifndef LAB7_QUEUE_H
#define LAB7_QUEUE_H


#include <queue>

class Buffer {
private:
    int round;
    std::queue<int> q;
    int addTailProb;
    int removeHeadProb;
    double averageLength=0;

public:
    /*!
     * Default Constructor that prompts User for parameters
     */
    Buffer();

    /*!
     * generate a random number between 1 and 1000
     */
    int generateNumber();

    /*!
     * add number to queue
     */
    void addTail();

    /*!
     * remove number from queue
     */
    void removeHead();

    /*!
     * Print the information of the buffer
     */
    void output(int currentRound);

    /*!
     * run rounds of buffer
     */
    void simulate();
};


#endif //LAB7_QUEUE_H
