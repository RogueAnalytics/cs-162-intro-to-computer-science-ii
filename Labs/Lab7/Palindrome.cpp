/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Palindrome.h"
#include "utility.h"

Palindrome::Palindrome() {
    std::string t;

    std::cout << "Enter Text to Make a Palindrome" << std::endl;
    std::cin >> t;

    for ( char c : t) {
        s.push(c);
    }
}

void Palindrome::output() {
    int numberInQueue = s.size();
    for( int i = 0; i < numberInQueue; i++){
        std::cout << s.top();
        s.pop();
    }
    std::cout << std::endl;
}
