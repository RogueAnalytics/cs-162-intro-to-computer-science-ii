#include <iostream>
#include "Buffer.h"
#include "Palindrome.h"
#include "utility.h"

int main() {
    int choice = 0;

    utility::getMinMaxInt(choice, 1, 3, "Menu\n\t1.) Test Buffer\n\t2.) Create a Palindrome\n\t3.) Quit", "Invalid Choice");
    while (choice != 3){
        if (choice == 1){
            Buffer b = Buffer();
            b.simulate();
        } else if (choice == 2){
            Palindrome p = Palindrome();
            p.output();
        }
        utility::getMinMaxInt(choice, 1, 3, "Menu\n\t1.) Test Buffer\n\t2.) Create a Palindrome\n\t3.) Quit", "Invalid Choice");
    }
    return 1;

}