/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB7_STACK_H
#define LAB7_STACK_H


#include <stack>

class Palindrome {
private:
    std::stack<char> s;
public:
    /*!
     * Default Constructor prompting user for word
     */
    Palindrome();

    /*!
     * Printing out the palindrome for the word
     */
    void output();


};


#endif //LAB7_STACK_H
