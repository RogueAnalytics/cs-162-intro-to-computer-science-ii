#include <iostream>
#include <fstream>
#include "utility.h"

int main() {
    const int size = 26;
    std::string fileName;

    // Gather File Name and Read File
    std::cout << "Enter File:";
    std::cin >> fileName;

    std::ifstream inputFile(fileName);

    //Check the file status and read
    if (inputFile) {
        int alphaarray[size];

        for(int i = 0; i < size; i++){
            alphaarray[i] = 0;
        }

        count_letters(inputFile, alphaarray);
    }
    else{
        inputFile.close();
        std::cout << "could not access file" << std::endl;
    }




}