/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB2_UTILITY_H
#define LAB2_UTILITY_H

#include <iostream>
#include <fstream>

void count_letters(std::ifstream &file, int* freq);
void output_letters(std::ofstream &file, int* freq);


#endif //LAB2_UTILITY_H
