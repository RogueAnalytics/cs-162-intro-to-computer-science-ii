/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "utility.h"
#include <iostream>


void count_letters(std::ifstream &file, int *freq) {
    std::string outFile;
    std::string line;
    std::locale loc;

    // Store the paragraph that is being shown.
    int paragraph;
    paragraph = 1;

    char lc;
    while (getline(file, line)){
        for(char& letter : line) {
            //lower the case and add the counter
            lc = std::tolower(letter, loc);
            freq[lc - 'a']++;
        }

        //ask user for the output file
        std::cout << "Enter Output File for Paragraph " << paragraph << ": ";
        std::cin >> outFile;

        // Write results to the output file
        std::ofstream outputFile(outFile);
        output_letters(outputFile, freq);
        outputFile.close();

        //Increase to second paragraph
        paragraph++;
    }



}

void output_letters(std::ofstream &file, int *freq) {
    char letter;
    //loop through alphabet array, print array, and reset the letter in the array
    for (int i = 0; i<26; i++){
        letter ='a'+ i;
        file << letter << ": " << freq[i] << std::endl;
        freq[i] = 0;
    }
}
