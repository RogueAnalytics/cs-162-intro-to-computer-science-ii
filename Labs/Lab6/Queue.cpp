/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "Queue.h"
#include "iostream"

bool Queue::isEmpty() {
    if (head == nullptr){
        return true;
    }
    return false;
}

void Queue::addBack(int val) {
    //Set up new node
    QueueNode *newNode = new QueueNode();
    newNode->val = val;

    if(isEmpty()){
        setHead(newNode);
    } else {
        newNode->prev = getNodeBack();
        newNode->next = getNodeHead();

        //Establish correct relationship with Link Lists
        getNodeBack()->next = newNode;
        getNodeHead()->prev = newNode;
    }
}

int Queue::getFront() {
    return head->val;
}

void Queue::removeFront(){
    if (!isEmpty()){
        QueueNode *h = head;
        QueueNode *n = head->next;
        QueueNode *p = head->prev;
        if(n != h){
            delete h;
            head = n;

            //Establish relationship with middle removed.
            p->next = n;
            n->prev = p;
        } else {
            delete h;
            head = nullptr;
        }
    }
}

void Queue::printQueue() {
    QueueNode *node;
    if(!isEmpty()){
        node = head;
        std::cout << "The contents of the queue are as follows:" << std::endl;
        do{
            std::cout << node->val << std::endl;
            node = node->next;
        } while (node != head);
    } else{
        std::cout << "Queue is Empty!!!" << std::endl;
    }
}

QueueNode *Queue::getNodeBack(){
    return head->prev;
}

QueueNode *Queue::getNodeHead() {
    return head;
}

void Queue::setHead(QueueNode *h) {
    if(h!=nullptr){
        if (isEmpty()){
            h->next = h;
            h->prev = h;
        } else{
            //Set previous link
            head->prev->next = h;
            h->prev = head->prev;

            //Set next link
            head->prev = h;
            h->next = head;
        }
    }
    head = h;
}

Queue::~Queue() {
    while(!isEmpty()){
        removeFront();
    }
}
