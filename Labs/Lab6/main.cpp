#include <iostream>
#include "Queue.h"
#include "utility.h"

int main() {
    Queue *q = new Queue();
    int choice;
    int val;

    do{
        utility::getMinMaxInt(choice, 1,5, "Please Select one of the following\n"
                                           "\t1.) Add a value to the back of queue\n"
                                           "\t2.) Display the front value\n"
                                           "\t3.) Remove the front node\n"
                                           "\t4.) Display the queue’s content\n"
                                           "\t5.) Exit", "Invalid Choice");

        if (choice == 1){
            std::cout << "Value to add to queue: ";
            utility::getInt(val, "Value to add to queue: ", "Invalid Choice");
            q->addBack(val);
        } else if (choice == 2){
            std::cout << "The front value is ";
            std::cout << q->getFront() << std::endl;

        } else if (choice == 3){
            q->removeFront();
        } else if (choice == 4){
            q->printQueue();
        }
    } while (choice != 5);

    delete  q;
    return 1;
}