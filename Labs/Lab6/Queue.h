/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB6_QUEUE_H
#define LAB6_QUEUE_H

struct QueueNode{
    QueueNode *next;
    QueueNode *prev;
    int val;
};

class Queue {
private:
    QueueNode *head;
public:
    /*!
     * Checks if the queue is empty. If so, returns true; otherwise, return false.
     * @return
     */
    bool isEmpty();
    /*!
     * Takes a user-inputted integer, creates a QueueNode with user-inputted integer, and appends it to the back of the list.

     * @param val
     */
    void addBack(int val);
    /*!
     * returns the value of the node at the front of the queue.
     * @return
     */
    int getFront();
    /*!
     * removes the front QueueNode of the queue and free the memory.
     */
    void removeFront();
    /*!
     * traverses through the queue from head using next pointers, and prints the values of each QueueNode in the queue.
     */
    void printQueue();
    /*!
     * returns last node
     */
    QueueNode *getNodeBack();
    /*!
     * return first node
     * @return
     */
    QueueNode *getNodeHead();
    /*!
     * set the first head
     * @return
     */
    void setHead(QueueNode *h);

    virtual ~Queue();
};


#endif //LAB6_QUEUE_H
