/*********************************************************************
** Program name: Utility Functions
** Author: W. Aaron Morris
** Date: 06/27/18
** Description: This file deals with the declaration of all the utility
 * functions that are going to be used during the course of CS162
*********************************************************************/

#ifndef LAB_1_UTILITY_H
#define LAB_1_UTILITY_H

#include <iostream>


namespace utility {
    void getInt(int &num, std::string q, std::string i);
    void getPositiveInt(int &num, std::string q, std::string i);
    void getMinMaxInt(int &num, int min, int max, std::string q, std::string i);
    void getFloat(float &num, std::string q, std::string i);
    void removeArrayItem(std::string *array, int &size, int removeItem);

};


#endif //LAB_1_UTILITY_H
