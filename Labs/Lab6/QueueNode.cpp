/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include "QueueNode.h"

QueueNode *QueueNode::getNext() const {
    return next;
}

void QueueNode::setNext(QueueNode *next) {
    QueueNode::next = next;
}

QueueNode *QueueNode::getPrev() const {
    return prev;
}

void QueueNode::setPrev(QueueNode *prev) {
    QueueNode::prev = prev;
}

int QueueNode::getVal() const {
    return val;
}

void QueueNode::setVal(int val) {
    QueueNode::val = val;
}

QueueNode::QueueNode(int val) {
    setVal(val);
}
