/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#ifndef LAB6_QUEUENODE_H
#define LAB6_QUEUENODE_H



class QueueNode {
private:
    QueueNode *next;
    QueueNode *prev;
    int val;
public:
    QueueNode *getNext() const;

    void setNext(QueueNode *next);

    QueueNode *getPrev() const;

    void setPrev(QueueNode *prev);

    int getVal() const;

    void setVal(int val);

    QueueNode(int val);
};


#endif //LAB6_QUEUENODE_H
