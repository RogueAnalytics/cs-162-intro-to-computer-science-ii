/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/


#include <iostream>
#include "utility.h"
#include "ReadMatrix.h"


void readMatrix(int **array, int size) {
    std::string text;
    for (int i = 0; i<size; i++){


        for (int j = 0; j<size; j++) {
            text = "Type in Number for Row:" + std::to_string(i+1);
            text += " Column:" + std::to_string(j+1);
            array[i][j] = utility::getint(text.c_str() , "Not a valid input");;
        }
    }
}
