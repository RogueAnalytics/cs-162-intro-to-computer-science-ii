/*********************************************************************
** Program name:
** Author:
** Date:
** Description:
*********************************************************************/

#include <iostream>
#include "utility.h"

namespace utility {

    int getint(const char* q, const char* i) {
        int num;
        std::cout << q << std::endl;

        while (!(std::cin >> num)){
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << i << ": " << std::endl;
        }
        return num;
    }
}

