/*********************************************************************
** Program name: Utility Functions
** Author: W. Aaron Morris
** Date: 06/27/18
** Description: This file deals with the declaration of all the utility
 * functions that are going to be used during the course of CS162
*********************************************************************/

#ifndef LAB_1_UTILITY_H
#define LAB_1_UTILITY_H


namespace utility {
    int getint(const char* q, const char* i);



};


#endif //LAB_1_UTILITY_H
