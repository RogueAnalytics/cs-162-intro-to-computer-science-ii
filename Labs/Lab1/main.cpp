/*********************************************************************
** Program name: Matrix Calculations
** Author: W. Aaron Morris
** Date: 06/26/18
** Description: This program reads a matrix from the user and prints the matrix
 * with the determinant to the console.
*********************************************************************/
#include "utility.h"

#include <iostream>
#include "ReadMatrix.h"
#include "Determinant.h"

int main() {
    int size = 0;

    while (size != 1 & size != 2) {
        size = utility::getint(
                "Select the size of your matrix \n 1. 2x2 \n 2. 3x3",
                "Not a valid input");
    }

    if (size == 1) {
        size = 2;
    } else if (size == 2) {
        size = 3;
    }

    int **matrix = new int *[size];

    for (int i = 0; i < size; i++) {
        matrix[i] = new int[size];
    }

    readMatrix(matrix, size);

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            std::cout << matrix[i][j];
            if (j < (size - 1)) {
                std::cout << "|";
            }
        }
        std::cout << std::endl;
    }

    std::cout << "Determinant is " << determinant(matrix, size) << std::endl;
    for(int i = 0; i < size; i++) {
        delete[] matrix[i];
	}
    delete[] matrix;
}
