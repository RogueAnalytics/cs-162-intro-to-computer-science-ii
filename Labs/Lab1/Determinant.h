/*********************************************************************
** Program name: Matrix Calculations
** Author: W. Aaron Morris
** Date: 06/26/18
** Description: This function finds the determinant of a matrix.
*********************************************************************/

#ifndef LAB_1_DETERMINANT_HPP
#define LAB_1_DETERMINANT_HPP


double determinant(int **matrix, int size);


#endif //LAB_1_DETERMINANT_HPP
