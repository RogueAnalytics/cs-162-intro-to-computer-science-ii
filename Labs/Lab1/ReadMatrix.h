/*********************************************************************
** Program name: MatrixCalc
** Author: W. Aaron Morris
** Date: 06/27/18
** Description: This function read a matrix via user input.
*********************************************************************/

#ifndef LAB_1_READMATRIX_HPP
#define LAB_1_READMATRIX_HPP


void readMatrix(int **array, int size);


#endif //LAB_1_READMATRIX_HPP
