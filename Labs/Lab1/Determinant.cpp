/*********************************************************************
** Program name: Matrix Calculations
** Author: W. Aaron Morris
** Date: 06/26/18
** Description: This function finds the determinant of a matrix.
*********************************************************************/

#include "Determinant.h"

double determinant(int **matrix, int size) {
    int rv;
    if (size == 2){
        rv = (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
    }
    else if (size == 3) {
        int v1 = matrix[0][0] *
                ((matrix[1][1] * matrix[2][2])-(matrix[1][2] * matrix[2][1]));

        int v2 = matrix[0][1] *
                 ((matrix[1][0] * matrix[2][2])-(matrix[2][0] * matrix[1][2]));

        int v3 = matrix[0][2] *
                 ((matrix[1][0] * matrix[2][1])-(matrix[2][0] * matrix[1][1]));

        rv =  v1 - v2 + v3;

    }

    return rv;
}
