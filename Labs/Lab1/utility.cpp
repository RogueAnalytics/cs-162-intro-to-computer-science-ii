/*********************************************************************
** Program name: Utility Functions
** Author: W. Aaron Morris
** Date: 06/27/18
** Description: This file deals with the implementation of all the utility
 * functions that are going to be used during the course of CS162
*********************************************************************/

#include <iostream>
#include "utility.h"

namespace utility {

    int getint(const char* q, const char* i) {
        int num;
        std::cout << q << std::endl;

        while (!(std::cin >> num)){
            std::cin.clear();
            std::cin.ignore(100, '\n');
            std::cout << i << ": " << std::endl;
        }
        return num;
    }
}

